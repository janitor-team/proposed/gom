README for

GOM (Gom is nOt yet another Mixer), a generic audio mixer.  Release
0.30 and prereleases.

Prereleases are versioned "0.29.x", with x being a non-negative
integer.

See NEWS for notes on the 0.30.0 release.


UPDATES
-------

Currently (2008-May), new versions may be downloaded from

http://software.installiert.net


ADMINISTRATIVE
--------------

Map of administrative files:

  - README   : overall/meta/misc information (this file)
  - AUTHORS  : list of all copyright holders on the package, or parts of the package
  - COPYING  : software licence for (this release of) gom
  - INSTALL  : GNU automake/autoconf generic installation descriptions
  - NEWS     : list of prominent changes between releases of gom
  - ChangeLog: detailed list of changes between releases of gom

Copyright (c) 1996-2009 Stephan Sürken <absurd@olurdix.de> [GPL].

(THIS RELEASE OF) GOM MAY ONLY BE DISTRIBUTED, COPIED OR MODIFIED
UNDER THE TERMS OF THE LICENSE FOUND IN THE FILE NAMED "COPYING"
INCLUDED IN THIS DISTRIBUTION.


ABSTRACT (from the manual page)
--------

.BR "gom" " is a command line mixer manipulation program including a
minimal, yet fully functional internal ineractive text based interface.

Currently, there is also an internal X (xview) interface, but it's not
well maintained and will eventually be removed when a proper alternative
is available.

At the moment, gom only supports the Open Sound System (OSS) and
its derivatives (OSS/Lite, OSS/Free (these two are obviously obsolete),
the new Linux Sounddriver, ...).

.BR "gom" " tries to provide a complete and convenient interface for all"
kind of audio mixer manipulation.  gom's facilities include sound
driver (compile time) and sound card (run time) independence,
arbitrary mixer selection, loading and saving of mixer settings,
volume fading, verbosity-level driven output, "Un*x-like scripting
support", etc.

Apart from the exhaustive command line interface described here, gom
has a built-in interactive terminal interface (that I call
gomii, gom interactive interface) using ncurses. It supports adjustable
(this includes disabling) real time updating. The gomii is not
explained in this manual page; please refer to the specific online
help when using it. However, the gomii's handling
should be obvious, and actually it "tries to resemble" the command
line options.

There is also one more gomii for X using the xview toolkit. However,
gom needs to be especially compiled to include this, and it is intended
to be replaced eventually by some frontend for X using the gom binary.

And remember: gom is spelled g-o-m, but pronounced backwards for
compatibility reasons. Its real, actual and recursive title is
.B gom, GOM is nOt yet another Mixer
(for reasons beyond the scope of this manual).


INSTALLATION
------------

Building and installing is being done by GNU automake/autoconf. Please
refer to the file "INSTALL" for generic installation descriptions, and
to the (special) notes produced by the ./configure script. Basically,
it checks for _some_ possible irregularities (i.e. those that I know
of) , but surely not all, so that a correct check does not necessarily
mean that compilation will succeed on your system.

However, to sum it up, it should be as simple as ($ == user shell,
# == root shell):

$ ./configure [<options>]
$ make
# [make install]

 KNOWN BUGS for the ./configure script: Essential warnings are
produced, but they are not complete (i.e. gom might still fail to
compile even without warnings). Gom basically is designed compile on a
POSIX.1 system with a C library conforming to ANSI C. GNU extensions
are welcome, and used if appropriate. At the moment, I knowingly use
the GNU extensions [v]snprintf _without_ patch for non-gnu
systems. Maybe I still unknowingly use some other GNU extensions.


DOCUMENTATION
-------------

* The manual page "gom.1" is the authoritative documentation source
  (e.g., "groff -man -Tascii gom.1 | less" views it without installing)
* "gom -h" == "gom --help" or 
  "gom -H" == "gom --help-verbose" gom's "online" help on options
  (completely included in the manual page, too, of course)


REQUIREMENTS, COMPATABILITY
---------------------------

(This section is out of date and may be inaccurate ;)

Mandatory:
* a Un*x system (whatever that is) with one of the following
  sound drivers installed:
  - Open Sound System (OSS) (all version claiming to instanciate the
    OSS API should work).
    The Linux kernel includes OSS/Free, so this should work on all
    Linux-based Un*x distributions.

Optional:
* a soundcard with mixer (this means that you can actually use gom...)
* ncurses (for the terminal gomii),
* xview toolkit (for the x gomii)

Well...:
At least, that's the theory. The only configurations successfully
tested by me are:
* ix86-Linux with kernel 2.0.x, and OSS/Free 3.5.4.
* ix86-Linux with kernel 2.1.5+, OSS/Free 3.7-beta (note: Hannu calls it
  "OSS Lite" in the Readme, neither "OSS/Free" nor "USS Lite", but I will
  stick to OSS/Free ;).

Other configurations or other configurations of the above configurations
might (and we all know: most likely will) fail to compile for whatever
unexpected reason.

Specials on the OSS driver:
Any improvements in the driver (e.g. if the new SB16 driver now
supports "mix") will of course directly affect gom; there should be no
need to update/recompile gom if you install a kernel with the new
driver. (Well, ok, for the purist: If the OSS API increases the amount
of possible channels (check SOUND_MIXER_NRDEVICES > 17 in soundcard.h)
-- which seems unlikely --, gom will support these (new channels) only
after recompilation with the new header file.)  However, if unsure,
recompile.
