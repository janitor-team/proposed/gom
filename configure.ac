dnl Copyright (c) Stephan Sürken 1996-2009

AC_INIT(gom, 0.30.4)
AC_CONFIG_SRCDIR(src/gom.c)
AM_INIT_AUTOMAKE

# Release: Package, Version, Date, Current date
DATE="31 May 2004"
DATE_CURRENT="`date +'%d %b %Y'`"
GOM_TITLE="GOM, Gom is nOt yet another Mixer"
GOM_COPYRIGHT="Copyright (c) 1996-2009 Stephan Sürken <absurd@olurdix.de> [GPL]."

AC_SUBST(DATE)
AC_SUBST(DATE_CURRENT)
AC_SUBST(GOM_TITLE)
AC_SUBST(GOM_COPYRIGHT)

AM_CONFIG_HEADER(config.h)

dnl Checks for programs.

AC_PROG_AWK
AC_PROG_CC
AC_PROG_INSTALL
AC_PROG_LN_S

AC_ARG_ENABLE(x-gomii,
              [--[dis|en]able-x-gomii    Dis- or enable X gomii. Disabled by default.])

AC_CHECK_LIB(ncurses, beep,, AC_MSG_ERROR("gom needs libncurses."))

if [[ "$enable_x_gomii" = "yes" ]]; then
	AC_PATH_XTRA

dnl autoconf does not seem to get x_includes right ;/
	GOM_CPPFLAGS="${GOM_CPPFLAGS} -I/usr/X11R6/include"
	GOM_CFLAGS="${GOM_CFLAGS} -L${x_libraries}"

	AC_CHECK_LIB(X11, main,   , AC_MSG_ERROR("x-gomii needs libX11."),   ${GOM_CFLAGS})
	AC_CHECK_LIB(olgx, main,  , AC_MSG_ERROR("x-gomii needs libolgx."),  ${GOM_CFLAGS})
	AC_CHECK_LIB(xview, main, , AC_MSG_ERROR("x-gomii needs libxview."), ${GOM_CFLAGS})
fi

AC_HEADER_STDC
AC_CHECK_HEADERS(unistd.h)
AC_CHECK_HEADERS(sys/soundcard.h machine/soundcard.h soundcard.h)
if [[ "$ac_cv_header_sys_soundcard_h" != "yes" ]] && [[ "$ac_cv_header_machine_soundcard_h" != "yes" ]] && [[ "$ac_cv_header_soundcard_h" != "yes" ]]; then
    AC_MSG_ERROR([No sound card header file.])
fi
AC_CHECK_HEADERS(fcntl.h limits.h getopt.h sys/ioctl.h sys/time.h)
AC_CHECK_HEADERS(ncurses.h curses.h)
if [[ "$ac_cv_header_ncurses_h" != "yes" ]] && [[ "$ac_cv_header_curses_h" != "yes" ]];
   then AC_MSG_ERROR([Neither curses.h nor ncurses.h found, needed by terminal gomii.])
fi

AC_C_CONST
AC_PROG_GCC_TRADITIONAL
AC_TYPE_SIGNAL
AC_FUNC_STRFTIME
AC_FUNC_VPRINTF
AC_CHECK_FUNCS(strerror)

dnl Custom preparations
AC_SUBST(GOM_CFLAGS)
AC_SUBST(GOM_CPPFLAGS)

dnl Selecting the sound device driver (currently only OSS)
GOM_SOUND_DRIVER_ID="oss"
AC_SUBST(GOM_SOUND_DRIVER_ID)

dnl C preprocesssor defines
if [[ "$enable_x_gomii" = "yes" ]]; then
  AC_DEFINE(GOM_ENABLE_X_GOMII, "", "Compile X gomii when this is defined.")
fi
AC_DEFINE_UNQUOTED(GOM_VERSION,   "$VERSION",       "Release version.")
AC_DEFINE_UNQUOTED(GOM_DATE,      "$DATE",          "Date of release.")
AC_DEFINE_UNQUOTED(GOM_TITLE,     "$GOM_TITLE",     "Project's title.")
AC_DEFINE_UNQUOTED(GOM_COPYRIGHT, "$GOM_COPYRIGHT", "Project's copyright line.")

dnl Finally create output

AC_OUTPUT(Makefile gom-${VERSION}.lsm:LSM.in src/Makefile src/gomconfig src/gom.1 src/gomconfig.8)
