/* gom, Gom is nOt yet another (Audio) Mixer.
 *
 * Copyright (C) 1996-2004  Stephan Sürken <absurd@olurdix.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * gom_info.c: output handling
 */

/*
 * INCLUDES
 */
#include "gom.h"

/*
 * MACROS
 */

/*
 * DECLARATIONS/DEFINITIONS
 */

/* holds verbosity mode for gom_info */
int gom_info_verbosity_var = GOM_INFO_NORMAL;

/* holds scroll mode for gom_info */
int gom_info_scroll_var = 0;

/* shown errors count definition */
int gom_info_errors = 0;

/* holds custom output procedure */
void (*gom_info_custom_var) (enum gom_info_types, char *, va_list) = gom_info_custom_default;

/*
 * FUNCTION PROTOTYPES
 */

/******************/

/*
 * VERBOSITY
 */

int gom_info_verbosity()
{
	return gom_info_verbosity_var;
}

void gom_info_verbosity_set(enum gom_info_types std_verb,
														int verbosity)
{
	int old_verbosity = gom_info_verbosity_var;
	gom_info_verbosity_var = verbosity;

	gom_info(std_verb,
					 "Verbosity set to %i (was %i) [" GOM_INFO_VERBOSITY_PRINTF_MASK "].",
					 gom_info_verbosity_var, old_verbosity,
					 GOM_INFO_VERBOSITY_PRINTF_VARS);
}

/*
 * SCROLL MODE
 */

int gom_info_scroll()
{
	return gom_info_scroll_var;
}

int gom_info_scroll_set(int scroll)
{
	int old_scroll = gom_info_scroll_var;
	gom_info_scroll_var = scroll;
	return old_scroll;
}

/*
 * CUSTOM OUTPUT PROC
 */

void gom_info_custom_default(enum gom_info_types kind, char * fmt, va_list vargs)
{
	FILE* output_stream;
	output_stream = stdout;
	if (kind == GOM_INFO_ERROR)
		output_stream = stderr;

	vfprintf(output_stream, fmt, vargs);
	fprintf(output_stream, "\n");
	fflush(output_stream);
}

void gom_info_custom_set(void (* proc) (enum gom_info_types, char *, va_list))
{
	if (proc == NULL)
		gom_info_custom_var = gom_info_custom_default;
	else
		gom_info_custom_var = proc;
}

/*
 * GOM INFO
 */

void gom_info(enum gom_info_types kind, char * fmt, ...)
{
	va_list vargs;
	char new_fmt[GOM_INFO_FMT_SIZE];

	/* error count */
	if ((kind == GOM_INFO_ERROR) && (gom_info_errors < INT_MAX))
		++gom_info_errors;

	if (kind <= gom_info_verbosity())
	{
		strncpy(new_fmt, (kind == GOM_INFO_ERROR) ? "gom ERROR: " : "gom: ", GOM_INFO_FMT_SIZE);
		strncat(new_fmt, fmt, GOM_INFO_FMT_SIZE);
		va_start(vargs, fmt);
		gom_info_custom_var(kind, new_fmt, vargs);
		va_end(vargs);
	}
}


/*
 * Two helpers
 */

#define GOM_INFO_BLOCK_LINE_SIZE 70
void gom_info_block(enum gom_info_types std_verb,
										int indent, char * text)
{
	char line[GOM_INFO_BLOCK_LINE_SIZE];
	int i;
	char * string = text;
	int block_width = GOM_INFO_BLOCK_LINE_SIZE-1-indent;

	do
	{
		/* avoid leading spaces */
		while (*string == ' ') string++;
		/* indent */
		strcpy(line, "");
		for (i=0; i < indent; i++)
			strncat(line, " ", GOM_INFO_BLOCK_LINE_SIZE-1);

		/* blocked text */
		strncat(line, string, block_width);
		/* show string */
		gom_info(std_verb, line);
		/* calc new string */
		if (strlen(string) > block_width)
			string += block_width;
		else
			break;
	}
	while (1);
}

void gom_info_text(enum gom_info_types kind, char * text[])
{
	int i;
	for (i = 0; text[i] != NULL; ++i)
		gom_info(kind, text[i]);
}
