/* gom, Gom is nOt yet another (Audio) Mixer.
 *
 * Copyright (C) 1996-2004  Stephan Sürken <absurd@olurdix.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * gom_info.h: output handling
 */

/*
 * INCLUDES
 */

/*
 * MACROS
 */

/* printf-like format string size */
#define GOM_INFO_FMT_SIZE 255

/* define this for debugging purposes only */
/* #define GOM_INFO_SHOW_ALL */

/* helps for printf-style output */
#define GOM_INFO_VERBOSITY_PRINTF_MASK "<=%i:ERROR, %i:QUIET, %i:NORMAL, %i:VERBOSE, >=%i:DEBUG"
#define GOM_INFO_VERBOSITY_PRINTF_VARS GOM_INFO_ERROR, GOM_INFO_QUIET, GOM_INFO_NORMAL, GOM_INFO_VERBOSE, GOM_INFO_DEBUG

/*
 * DECLARATIONS
 */

/* the gom_info message types */
enum gom_info_types {GOM_INFO_ERROR=-1, GOM_INFO_QUIET, GOM_INFO_NORMAL, GOM_INFO_VERBOSE, GOM_INFO_DEBUG};

/* shown errors count */
extern int gom_info_errors;

/*
 * FUNCTION PROTOTYPES
 */

/*
 * VERBOSITY
 */
int gom_info_verbosity();
void gom_info_verbosity_set(enum gom_info_types std_verb, int verbosity);

/*
 * SCROLL MODE
 */
int gom_info_scroll();
int gom_info_scroll_set(int scroll);

/*
 * CUSTOM OUTPUT PROC
 */
void gom_info_custom_default(enum gom_info_types kind, char * fmt, va_list vargs);
void gom_info_custom_set(void (* proc) (enum gom_info_types, char *, va_list));

/*
 * GOM INFO
 */
void gom_info(enum gom_info_types kind, char * fmt, ...);
void gom_info_block(enum gom_info_types std_verb, int indent, char * text);
void gom_info_text(enum gom_info_types kind, char * text[]);
