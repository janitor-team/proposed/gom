/* gom, Gom is nOt yet another (Audio) Mixer.
 *
 * Copyright (C) 1996-2004  Stephan Sürken <absurd@olurdix.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * gom_iface_c.c: command line interface
 */

/*
 * INCLUDES
 */
#include "gom.h"

/*
 * MACROS
 */

/*
 * DECLARATIONS/DEFINTIONS
 */

/*
 * FUNCTION PROTOTYPES
 */

/*****************/

/*
 * COMMAND LINE INTERFACE
 */

char *
gom_iface_c_get_option_args(char * prompt)
{
	return optarg;
}

int
gom_iface_c_question(char * question)
{
	char answer;
	gom_info(GOM_INFO_QUIET, "%s %s ", "[y|Y = yes, else no]", question);
	scanf("%c", &answer);
	return (toupper(answer) == 'Y');
}

/* the mother of all gom functions. */

void
gom_iface_c(int argc, char *argv[])
{
	int action = 0;
	int ignore_config = 0;

	/* set the default for message writing routines (gom_info) */
	gom_info_custom_set(NULL);

	if (argc == 1)
	{
		/* called with no options */
		gom_info(GOM_INFO_QUIET, "You must specify an option.");
		gom_info(GOM_INFO_QUIET, GOM_ACTION_HELPHINT);
	}
	else
	{
		/* read very first argument */
		action = gom_action_getopt(argc, argv);

		/* loop for all actions that should be taken before loading the auto configuration files */
		/* these are all options that do not affect any mixer settings */
		/* explicitely: quiet, verbosity, ignore-config, version, copyright, help, help-verbose */
		while (action == 'v' || action == 'q' || action == 'Y' || action == 'V' || action == 'w' ||
					 action == 'h' || action == 'H')
		{
			if (action == 'Y')
				ignore_config = 1;
			else
				gom_action(GOM_INFO_NORMAL, action, 1, gom_iface_c_get_option_args, NULL, NULL);
			action = gom_action_getopt(argc, argv);
		}

		/* automatic configuration; skipped if a) explicitely told so already, or */
		/* b) there are no more options (=> no mixer affecting options at all) */
		if (ignore_config)
			gom_info(GOM_INFO_NORMAL, "Explicitely ignoring automatic configuration '"
							 GOM_CONFIG_FILE "' and '" GOM_DEFAULT_MIXER_FILE "'.");
		else if (action != EOF)
		{
			gom_file_load (GOM_INFO_VERBOSE, GOM_CONFIG_FILE,
										 0, GOM_CONFIG_FILE_ALLOWED);
			gom_file_load (GOM_INFO_VERBOSE, GOM_DEFAULT_MIXER_FILE,
										 0, GOM_DEFAULT_MIXER_FILE_ALLOWED);
		}

		/* now do the main loop */
		while (action != EOF)
		{
			gom_action(GOM_INFO_NORMAL, action, 1, gom_iface_c_get_option_args, NULL, NULL);
			action = gom_action_getopt(argc, argv);
		}
	}
}
