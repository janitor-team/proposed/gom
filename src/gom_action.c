/* gom, Gom is nOt yet another (Audio) Mixer.
 *
 * Copyright (C) 1996-2004  Stephan Sürken <absurd@olurdix.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * gom_action: options, standard action, misc
 */

/*
 * INCLUDES
 */
#include "gom.h"

/*
 * MACROS
 */

#define GOM_INFO_CHANNEL_INFO_LINE_HEADER  "    No Name       Rec  Volumes  Vol-0  Vol-1  Locked"
/*                                         "    dd.ssssssssss dd     dd      ddd    ddd    dd   " */
#define GOM_INFO_CHANNEL_INFO_LINE_MASK    "    %2d.%-10s %2d     %2d      %3d    %3d    %2d"
#define GOM_INFO_TABLE_LINE                "    ------------------------------------------------"

/*
 * DECLARATIONS/DEFINTIONS
 */

/* if set, gom_help will output this too */
void * gom_action_help_special_var = NULL;

/*
 * FUNCTION PROTOTYPES
 */

/*****************/

/*
 * STANDARD OPTIONS
 */

/* name, has_arg (0,1,2), *flag, val */
struct option gom_options[] =
{
	/* CONFIGURING OPTIONS */
	{"device",              required_argument, 0, 'd'},
	{"mixer",               required_argument, 0, 'd'},
	{"channel",             required_argument, 0, 'c'},
	{"volume-channel",      required_argument, 0, 'C'},
	{"lock",                required_argument, 0, 'k'},
	{"lock-all",            required_argument, 0, 'K'},

	{"fade-interval",       required_argument, 0, 'F'},
	{"refresh-interval",    required_argument, 0, 'U'},
	{"write-config",        no_argument,       0, 'W'},
	{"save-config",         no_argument,       0, 'W'},

	/* SETTING MIXER OPTIONS */
	{"loudness",            required_argument, 0, 'l'},
	{"volume",              required_argument, 0, 'l'},
	{"record",              required_argument, 0, 'r'},
	{"record-single",       no_argument,       0, 'R'},

	{"fade-to-loudness",    required_argument, 0, 'L'},
	{"fade-to-volume",      required_argument, 0, 'L'},
	{"mute",                no_argument,       0, 'm'},
	{"mute-all",            no_argument,       0, 'M'},

	/* MIXERS SETTINGS OPTIONS */
	{"get-options",         optional_argument, 0, 'G'},
	{"load-options",        optional_argument, 0, 'G'},
	{"get-settings",        optional_argument, 0, 'G'},
	{"load-settings",       optional_argument, 0, 'G'},
	{"save-settings",       optional_argument, 0, 'S'},
	{"snapshot-settings",   no_argument,       0, 'z'},
	{"unsnapshot-settings", no_argument,       0, 'Z'},
	{"restore-settings",    no_argument,       0, 'Z'},
	{"originate",           no_argument,       0, 'O'},
	{"initialize",          no_argument,       0, 'O'},

	/* INFORMATIONAL OPTIONS */
	{"info",                no_argument,       0, 't'},
	{"info-all",            no_argument,       0, 'T'},
	{"version",             no_argument,       0, 'V'},
	{"copyright",           no_argument,       0, 'w'},
	{"copyleft",            no_argument,       0, 'w'},
	{"license",             no_argument,       0, 'w'},
	{"warranty",            no_argument,       0, 'w'},
	{"help",                no_argument,       0, 'h'},
	{"help-verbose",        no_argument,       0, 'H'},

	/* SPECIAL OPTIONS */
	{"execute",             required_argument, 0, 'e'},

	/* COMMAND LINE ONLY OPTIONS */
	{"ignore-config",       no_argument, 0, 'Y'},
	{"interface",           required_argument, 0, 'i'},
	{"gomii",               required_argument, 0, 'i'},
	{"verbose",             optional_argument, 0, 'v'},
	{"quiet",               no_argument, 0, 'q'},
	{"silent",              no_argument, 0, 'q'},
	{"extract-settings",    no_argument, 0, 'x'},
	{"read-stdin",          no_argument, 0, 'I'},

	/* NEW print-* OPTIONS */
	{"print-driver",           no_argument, 0, '1'},
	{"print-driver-channels",  no_argument, 0, '2'},

	{"print-device",           no_argument, 0, '3'},
	{"print-mixer",            no_argument, 0, '3'},

	{"print-channels",         no_argument, 0, '4'},
	{"print-channel-name",     no_argument, 0, '5'},
	{"print-channel-label",    no_argument, 0, '6'},
	{"print-channel-volumes",  no_argument, 0, '7'},
	{"print-channel-volume",   no_argument, 0, '8'},
	{"print-channel-recsrc",   no_argument, 0, '9'},
	{"print-channel-lock",     no_argument, 0, '0'},

	{"print-fade-interval",    no_argument, 0, 'a'},
	{"print-refresh-interval", no_argument, 0, 'A'},
	{NULL, 0, 0, 0}
};

/* name, has_arg (0,1,2), *flag, val */
char * gom_options_help[] =
{
	/* BASIC CONFIGURING OPTIONS */
	/* device */
	NULL,
	/* mixer */
	"[" GOM_DEVICE_NOT_OPENED_TEXT "] Set mixer special device file to <argument>. "
	"If the new mixer is valid, the current mixer --if any-- will be closed and "
	"the new mixer opened. Current channel, current channel volume, the channel "
	"lock setting and the snapshot will be resetted to defaults.",
	/* channel  */
	"[first available channel] Set current mixer channel to <argument>. "
	"The channel may be given as number or as name.",
	/* volume-channel  */
	"[first available volume channel on current channel] Set volume channel on current mixer channel "
	"to <argument> (e.g., for stereo, 0 means left, 1 means right volume).",
	/* lock  */
	NULL,
	/* lock-all  */
	"[1] Lock or unlock current or all channel(s). Locking means syncing of the stereo volumes "
	"(balance) for all volume settings gom might do -- this doesn't change any volume "
	"settings by itself (i.e., it doesn't auto-balance). Thus, a locked channel might have unbalanced "
	"volumes.",

	/* fade-interval */
	"[" GOM_MIXER_FADIVAL_DEFAULT_QUOTED "] Set fade interval to <argument> seconds. See --fade-to-loudness.",
	/* refresh-interval */
	"[" GOM_GOMII_REFIVAL_DEFAULT_QUOTED "] Set gomii refresh (update) interval to "
	"<argument> seconds (zero disables).",
	/* write-config */
	NULL,
	/* save-config */
	"This option is obsolete since version 0.29.10.",

	/* SETTING MIXER OPTIONS */
	/* volume  */
	NULL,
	/* loudness  */
	"Set current volume channel on current channel to <argument>. "
	"If the argument is being given with a leading \"+\" or \"-\", the given value will be added or "
	"substracted, respectively, from the current value. The allowed range is from zero up to a soundcard driver "
	"dependent maximum.",
	/* record  */
	"Set recording for current channel on or off.",
	/* record-single  */
	"Set recording for current channel on and disable all other recording sources.",

	/* fade-to-volume  */
	NULL,
	/* fade-to-loudness  */
	"Like --loudness, but fade to the new volume within a time given with --fade-interval.",
	/* mute */
	NULL,
	/* mute-all */
	"Mute current or all channels. Muting means setting all channel volumes to 0.",

	/* MIXERS SETTINGS OPTIONS */
	/* get-options */
	NULL,
	/* load-options */
	NULL,
	/* get-settings */
	NULL,
	/* load-settings */
	"Get options from/to file <argument>. If no argument is given, the default file "
	"(named \"" GOM_OPTIONS_FILE_DEFAULT "\") is used. "
	"Non-absolut given filenames will be expanded to \"<mixer-device>.<argument>\", and "
	"then first searched for in the user and -- if this fails -- in the system "
	"configuration directory. "
	"Any free-form files with gom one-character command line options in any lines starting "
	"with a dash (in column zero) will make sense to this option.",
	/* save-settings */
	"Save mixer settings to a free-form option file; for the file name, the same rules as for "
	"loading option files apply, except that only the user configuration dir will be used. "
	"Files with thusly expanded filenames will be silently overwritten; "
	"other files never. When saving, care is being taken that "
	"the \"last recording source error\" can't occur when loading these options (and maybe "
	"there are other reasonable side effects apart from the pure mixer settings (e.g. channel locking, "
	"current channel)).",
	/* snapshot-settings */
	NULL,
	/* unsnapshot-settings */
	NULL,
	/* restore-settings */
	"[mixer settings after opening a new mixer] Snap- or unsnapshot to/from current mixer settings.",
	/* originate */
	NULL,
	/* initialize */
	"Load the options file " GOM_INIT_FILE "; all options are allowed in this file. This is "
	"meant to initialize mixers. For example: \"-d/dev/mixer0 -G -d/dev/mixer1 -G\". This would load the "
	"default settings file for both the mixer0 and the mixer1 device.",

	/* INFORMATIONAL OPTIONS */
	/* info */
	"Display current channel information.",
	/* info-all */
	"Display overall information.",
	/* version */
	"Display version information.",
	/* copyright */
	NULL,
	/* copyleft */
	NULL,
	/* license */
	NULL,
	/* warranty */
	"Display copyright/license/warranty information.",
	/* help */
	NULL,
	/* help-verbose */
	"Display this help normally or verbose; both helps are still dependent on the "
	"current verbosity level (i.e., higher verbosity levels might still show more; "
	"\"gom -v0 -H\" and \"gom -h\" produce the same output). "
	"For the normal verbosity level, these are reasonable macros.",

	/* SPECIAL OPTIONS */
	/* execute */
	"Execute the shell command <argument>.",

	/* COMMAND LINE ONLY OPTIONS */
	/* ignore-config */
	"Skip all automatically loaded configurations files; this must be given "
	"before any other option (except q (quiet) or v (verbose)).",
	/* interface */
	NULL,
	/* gomii */
	"Explicitly start up a build-in gomii (<argument>=t: terminal gomii, <argument>=x: X gomii).",
	/* verbose */
	"[NORMAL] Set output verbosity level to <argument> (number, the higher, the more verbose). If no "
	"argument is given, the level will be increased by 1.",
	/* quiet */
	NULL,
	/* silent */
	"Set output verbosity to QUIET (only error / error help messages).",
	/* extract-settings */
	"Extract all mixer settings as a gom option line to stdout (e.g. for \"setting=`gom --quiet --extract-settings`\" "
	"and \"gom --quiet $settings\" later in a shell script).",
	/* read-stdin */
	"Read options from stdin (until EOF).",

	/* ******************* */
	/* NEW print-*-options */
	/* print-driver */
	"Print the used sound driver.",
	/* print-driver-channels */
	"List of all channels known by the used driver.",

	/* print-device */
	NULL,
	/* print-mixer */
	"Prints current mixer's device file name.",

	/* print-channels */
	"Prints a list of all currently available channels.",
	/* print-channel-name */
	"Prints current channel's name.",
	/* print-channel-label */
	"Prints current channel's label.",
	/* print-channel-volumes */
	"Prints a list of all available volumes of current channel.",
	/* print-channel-volume (replaces --print, --pure, -p) */
	"Print the current subchannel's volume value.",
	/* print-channel-recsrc */
	"Prints current channel's record state (-1=unavailable, 0=off, 1=on).",
	/* print-channel-lock */
	"Prints current channel's lock state (0=unlocked, 1=locked).",

	/* print-fade-interval */
	"Prints current fade interval.",
	/* print-refresh-interval */
	"Prints current refresh interval."
};

/* compute short options from long options */
char * gom_action_shortopts()
{
	static char shortopts[120] = "";
	char shortopts_copy[120] = "";
	int i;

	strcpy(shortopts, "");
	for (i = 0; gom_options[i].name != NULL; ++i)
	{
		strcpy(shortopts_copy, shortopts);
		snprintf(shortopts, 120,
		         "%s%c%s", shortopts_copy, gom_options[i].val,
		         gom_options[i].has_arg == optional_argument ? "::" : (gom_options[i].has_arg == required_argument ? ":" : ""));
	}
	return shortopts;
}

/* compute short options from long options */
char gom_action_shortopt(char * longopt)
{
	int i;

	for (i = 0; gom_options[i].name != NULL; ++i)
	{
		if (strcmp(gom_options[i].name, longopt) == 0)
			return gom_options[i].val;
	}
	return -1;
}

int gom_action_getopt(int argc, char * const argv[])
{
	int action = -1;

#if defined(HAVE_GETOPT_H)
	int option_index;
	action = getopt_long(argc, argv, gom_action_shortopts(), gom_options, &option_index);
#else /* not HAVE_GETOPT_H */
	action = getopt(argc, argv, gom_action_shortopts());
#endif /* not HAVE_GETOPT_H */

	return action;
}



/*
 * VERSION, LICENSE, HELP
 */

void gom_action_version(enum gom_info_types std_verb)
{
	gom_info(std_verb, GOM_VERSION_STRING);
}

void gom_action_license(enum gom_info_types std_verb)
{
	char * license_text[]=
		{
			"",
			"Copyright/Licence",
			"-----------------",
			"",
			"  " GOM_TITLE,
			"  " GOM_VERSION_STRING,
			"  " GOM_COPYRIGHT,
			"  Compile time: " __DATE__ " " __TIME__,
			"",
			"  This program is free software; you can redistribute it and/or modify",
			"  it under the terms of the GNU General Public License as published by",
			"  the Free Software Foundation; either version 2 of the License, or",
			"  (at your option) any later version.",
			"  ",
			"  This program is distributed in the hope that it will be useful,",
			"  but WITHOUT ANY WARRANTY; without even the implied warranty of",
			"  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the",
			"  GNU General Public License for more details.",
			"  ",
			"  You should have received a copy of the GNU General Public License",
			"  along with this program; if not, write to the Free Software",
			"  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.",
			NULL
		};
	gom_info_text(std_verb, license_text);
}


#define GOM_HELP_OPTION_LINE_SIZE 70
void gom_help_option(enum gom_info_types std_verb, int * option)
{
	char line[GOM_HELP_OPTION_LINE_SIZE];
	char line_copy[GOM_HELP_OPTION_LINE_SIZE];
	int i;

	/* get one-char-option */
	snprintf(line, GOM_HELP_OPTION_LINE_SIZE, "    -%c", gom_options[*option].val);
	/* get all long options */
	for (i=*option; gom_options[i].val == gom_options[*option].val; i++)
	{
		strcpy(line_copy, line);
		snprintf(line, GOM_HELP_OPTION_LINE_SIZE, "%s, --%s", line_copy, gom_options[i].name);
	}
	*option = --i;

	/* show (optional) argument */
	switch(gom_options[i].has_arg)
	{
	case no_argument:
		break;
	case required_argument:
		strcpy(line_copy, line);
		snprintf(line, GOM_HELP_OPTION_LINE_SIZE, "%s <argument>", line_copy);
		break;
	case optional_argument:
		strcpy(line_copy, line);
		snprintf(line, GOM_HELP_OPTION_LINE_SIZE, "%s [<argument>]", line_copy);
		break;
	default:
		break;
	}

	gom_info(std_verb, line);

	if (gom_options_help[i])
		gom_info_block(std_verb+1, 8, gom_options_help[i]);
}


void gom_action_help(enum gom_info_types std_verb)
{
	int i;
	char * help_text[]=
		{
			"",
			"Help on command line options",
			"----------------------------",
			"",
			"  Usage: gom {OPTION}",
			NULL
		};

	if (gom_action_help_special_var != NULL)
	{
		gom_info_text(std_verb, gom_action_help_special_var);
	}
	gom_info_text(std_verb, help_text);

#if !defined(HAVE_GETOPT_H)
	gom_info(std_verb, "");
	gom_info_block(std_verb, 4, "WARNING: GNU-style long options are disabled (no <getopt.h> found).");
#endif /* not HAVE_GETOPT_H */

	gom_info(std_verb+1, "");
	gom_info_block(std_verb+1, 4,
	               "Options can be given in arbitrary order or amount; they are "
	               "computed in sequence from left to right. Default values (if any), "
	               "are given in []. For boolean arguments, "
	               "\"1\" means on, \"0\" means off.");

	gom_info(std_verb+1, "");
	gom_info_block(std_verb+1, 4,
	               "Note that for options with _optional_ arguments, these must be given "
	               "like \"gom -G<file>\" (or \"gom --get-settings=<file>\" resp.) rather "
	               "than \"gom -G <file>\" (or \"gom --get-settings <file>\" resp.). "
	               "Otherwise, they will be ignored (or, at least with my implementation of getopt;).");

	for (i=0; gom_options[i].name != NULL; i++)
	{
		/* hack new lines for help headlines ... */
		switch (gom_options[i].val)
		{
		case 'd':
		case 'l':
		case 'G':
		case 't':
		case 'e':
		case 'Y':
			gom_info(std_verb, "");
			break;
		default:
			break;
		}
		/* hack the right help headlines ... */
		switch (gom_options[i].val)
		{
		case 'd':
			gom_info(std_verb, "  Configuring options:");
			break;
		case 'l':
			gom_info(std_verb, "  Setting mixer options:");
			break;
		case 'G':
			gom_info(std_verb, "  Mixer settings options:");
			break;
		case 't':
			gom_info(std_verb, "  Informational options:");
			break;
		case 'e':
			gom_info(std_verb, "  Special options:");
			break;
		case 'Y':
			gom_info(std_verb, "  Command line only options:");
			break;
		default:
			break;
		}
		gom_help_option(std_verb, &i);
	}
	if (std_verb > GOM_INFO_QUIET)
	{
		gom_info(std_verb, "");
		gom_info(std_verb, "`gom --help-verbose' might give more information.");
	}
}

void gom_action_help_special_set(char * help_text[])
{
	gom_action_help_special_var = help_text;
}


/*
 * Some standard output
 */

void gom_info_info_help(enum gom_info_types std_verb,
                   int c, int put_info)
{
	if (put_info)
	{
		gom_info(std_verb, "");
		gom_info(std_verb, "Current channel information");
		gom_info(std_verb, "---------------------------");
		gom_info(std_verb, "");
		gom_info(std_verb, GOM_INFO_CHANNEL_INFO_LINE_HEADER);
	}

	gom_info(std_verb,
	         GOM_INFO_CHANNEL_INFO_LINE_MASK,
	         c, gom_driver_C_name (c, 0),
	         gom_driver_c_r (c),
	         gom_driver_c_c_len (c),
	         gom_driver_c_c_v (c, 0),
	         gom_driver_c_c_v (c, 1),
	         gom_mixer_c_l (c));
}

void gom_action_current_channel_info(enum gom_info_types std_verb)
{
	gom_info_info_help(std_verb, gom_mixer_c_current(), 1);
}

void gom_action_overall_info(enum gom_info_types std_verb)
{
	int c;
	char * runtime_text[]=
		{
			"",
			"Overall information",
			"-------------------",
			"",
			NULL
		};
	gom_info_text(std_verb, runtime_text);

	gom_info(std_verb, "  Configuration:");
	gom_info(std_verb,
	         "    Mixer special file: %s.",
	         gom_driver_d());
	gom_info(std_verb,
	   "    Fade duration     : %d seconds.",
	   gom_mixer_fadival());
	gom_info(std_verb,
					 "    Gomii refresh     : all %d seconds%s.",
					 gom_gomii_refival(), gom_gomii_refival() == 0 ? " (disabled)" : "");

	gom_info(std_verb,
					 "    Verbosity         : %i [" GOM_INFO_VERBOSITY_PRINTF_MASK "].",
					 gom_info_verbosity(),
					 GOM_INFO_VERBOSITY_PRINTF_VARS);
	gom_info(std_verb,
					 "    Lock mask         : (see table below).");
	gom_info(std_verb,
					 "    Errors detected   : %i.",
					 gom_info_errors);

	gom_info(std_verb, "");
	gom_info(std_verb, "  Current mixer information:");
	gom_info(std_verb,
					 "    Driver used       : %s",
					 gom_driver());
	gom_info(std_verb,
					 "    Available channels: %d / %d",
					 gom_driver_c_len(), gom_driver_C_len());

	gom_info(std_verb, GOM_INFO_TABLE_LINE);
	gom_info(std_verb, GOM_INFO_CHANNEL_INFO_LINE_HEADER);
	gom_info(std_verb, GOM_INFO_TABLE_LINE);

	for (c = gom_driver_c_first(); c > -1; c = gom_driver_c_next(c, 1))
	{
		gom_info_info_help(std_verb, c, 0);
	}
	gom_info(std_verb, GOM_INFO_TABLE_LINE);
	for (c = gom_driver_C_first(); c > -1; c = gom_driver_C_next(c, 1))
	{
		if (!(gom_driver_c (c) > -1)) gom_info_info_help(std_verb, c, 0);
	}
	gom_info(std_verb, GOM_INFO_TABLE_LINE);
	gom_info(std_verb, "    -1 = unavailable, 1 = yes (or value), 0 = no (or value), ");
}

/*
 * Misc tools
 *
 */

int gom_action_arg2i(char * str, int * i)
{
	if ((str) && (1 == sscanf(str, "%d", i)))
		return 1;
	else
	{
		gom_info(GOM_INFO_ERROR,
						 "Option argument \"%s\" ignored: can't convert properly into integer.",
						 str ? str : "<NULL>");
		return 0;
	}
}

/* handles +/- prefix before the number in str */
int gom_action_arg2v(char * str, int old_v, int * v)
{
	if (gom_action_arg2i(str, v))
	{
		if (*str == '+' || *str == '-')
			*v += old_v;
		return 1;
	}
	else    /* false conversion */
		return 0;
}

/*
 * GOM STANDARD ACTIONS
 */

enum gom_gomii_refresh_stage
gom_action(enum gom_info_types std_verb,
					 char action, int cmd_line,
					 char * (* get_option_arg) (char *),
					 void (* pre_scroll) (), void (* post_scroll) ())
{
	int new_int, c, cc;
	char * local_optarg;
	int execute_err;
	enum gom_gomii_refresh_stage stage = GOM_GOMII_REFRESH_CURRENT_CHANNEL; /* this is the default */

	switch (action)
	{
	case 'd':
		/* Mixer device */
		local_optarg = get_option_arg("New mixer device? ");
		if (gom_driver_d_set(std_verb, local_optarg))
		{
			gom_mixer_d_defaults_set(std_verb+1);
			stage = GOM_GOMII_REFRESH_INIT;
		}
		else
			stage = GOM_GOMII_REFRESH_NO;
		break;

	case 'c':
		/* Current channel */
		local_optarg = get_option_arg("New current channel? ");

		if (isdigit(*local_optarg) && (gom_action_arg2i (local_optarg, &new_int)))
		{
			/* channel given as number "-c1" */
			gom_mixer_c_current_set(std_verb, new_int);
		}
		else
		{
	    /* channel given as string "-cbass" */
	    gom_mixer_c_current_set(std_verb, gom_mixer_C_find(local_optarg));
	  }

		stage = GOM_GOMII_REFRESH_CHANNELS;
		break;

	case 'C':
		/* Volume channel */
		if (gom_action_arg2i (get_option_arg("New current volume channel? "), &new_int))
			gom_mixer_c_c_current_set(std_verb, new_int);
		break;

	case 'k':
		/* Channel lock */
		if (gom_action_arg2i (get_option_arg("Lock current channel (0|1)? "), &new_int))
			gom_mixer_c_l_set(std_verb, gom_mixer_c_current(), new_int);
		break;

	case 'K':
		/* All Channel lock */
		if (gom_action_arg2i (get_option_arg("Lock all channels (0|1)? "), &new_int))
		{
			gom_mixer_l_set(std_verb, new_int);
			stage = GOM_GOMII_REFRESH_CHANNELS;
		}
		break;

	case 'F':
		/* Fade interval */
		if (gom_action_arg2i (get_option_arg("New fade interval? "), &new_int))
			gom_mixer_fadival_set(std_verb, new_int);
		stage = GOM_GOMII_REFRESH_NO;
		break;

	case 'U':
		/* Refresh interval */
		if (gom_action_arg2i (get_option_arg("New gomii refresh interval? "), &new_int))
			gom_gomii_refival_set (std_verb,
														 new_int,
														 gom_gomii_refproc_keep);
		stage = GOM_GOMII_REFRESH_NO;
		break;

	case 'W':
		/* Save config */
		gom_info(GOM_INFO_ERROR, "Since version 0.29.10, automatic saving of configuration is no longer possible (edit it by hand!).");
		stage = GOM_GOMII_REFRESH_NO;
		break;

	case 'l':
		/* Volume */
		if (gom_action_arg2v (get_option_arg("New volume? "),
													gom_driver_c_c_v(gom_mixer_c_current(), gom_mixer_c_c_current()),
													&new_int))
			gom_mixer_c_c_v_set_l(std_verb,
														gom_mixer_c_current(), gom_mixer_c_c_current(),
														new_int);
		break;

	case 'r':
		/* Recording */
		if (gom_action_arg2i (get_option_arg("Make channel recording source (0,1)? "), &new_int))
			gom_driver_c_r_set(std_verb,
												 gom_mixer_c_current(), new_int);
		break;

	case 'R':
		/* Recording single */
		gom_mixer_c_r_set_single(std_verb, gom_mixer_c_current());
		stage = GOM_GOMII_REFRESH_CHANNELS;
		break;

	case 'L':
		/* Fade */
		if (gom_action_arg2v (get_option_arg("New volume to fade to? "),
													gom_driver_c_c_v(gom_mixer_c_current(), gom_mixer_c_c_current()),
													&new_int))
			gom_mixer_c_c_v_fade_l(std_verb,
														 gom_mixer_c_current(), gom_mixer_c_c_current(),
														 new_int);
		break;

	case 'm':
		/* Mute channel */
		gom_mixer_c_mute(std_verb,
										 gom_mixer_c_current());
		break;
	case 'M':
		gom_mixer_mute(std_verb);
		stage = GOM_GOMII_REFRESH_CHANNELS;
		break;

	case 'G':
		/* Get options from options file */
		gom_file_load(std_verb,
									get_option_arg("Option file to load from [" GOM_OPTIONS_FILE_DEFAULT "]? "),
									1, NULL);
		stage = GOM_GOMII_REFRESH_CHANNELS;
		break;
	case 'S':
		/* Save settings as options file */
		gom_file_settings_save(std_verb, get_option_arg("Save settings as options file to [" GOM_OPTIONS_FILE_DEFAULT "]? "));
		stage = GOM_GOMII_REFRESH_NO;
		break;

	case 'z':
		/* Znapshot */
		gom_mixer_snapshot(std_verb);
		stage = GOM_GOMII_REFRESH_NO;
		break;
	case 'Z':
		/* UnZnapshot */
		gom_mixer_unsnapshot(std_verb);
		stage = GOM_GOMII_REFRESH_CHANNELS;
		break;

	case 'O':
		/* initialize */
		gom_file_load (std_verb, GOM_INIT_FILE, 0, NULL);
		stage = GOM_GOMII_REFRESH_INIT;
		break;

	case 't':
	case 'T':
	case 'V':
	case 'w':
	case 'h':
	case 'H':
		if (pre_scroll != NULL) pre_scroll();
		switch (action)
		{
		case 't':
			gom_action_current_channel_info(std_verb);
			break;
		case 'T':
			gom_action_overall_info(std_verb);
			break;
		case 'V':
			gom_action_version(std_verb);
			break;
		case 'w':
			gom_action_license(std_verb);
			break;
		case 'h':
			gom_action_help(std_verb);
			break;
		case 'H':
			/* show _complete_ help */
			gom_action_help(GOM_INFO_QUIET);
			break;
		default:
			break;
		}
		if (post_scroll != NULL) post_scroll();
		stage = GOM_GOMII_REFRESH_PAGE;
		break;

		/* Special options */

	case 'e':
		/* Executer (using /bin/sh via system()) */
		local_optarg = get_option_arg("System command to execute? ");
		gom_info(std_verb,
						 "Executing shell command \"%s\".", local_optarg);
		execute_err = system(local_optarg);
		if (execute_err != 0)
			gom_info(GOM_INFO_ERROR,
							 "Shell command \"%s\" returned with error code %i.",
							 local_optarg, execute_err);
		else
			gom_info(std_verb,
							 "Shell command \"%s\" successfully finished.",
							 local_optarg);

		stage = GOM_GOMII_REFRESH_NO;
		break;

		/* Special special actions (not listet) */

	case '\f':
		/* explicit redraw */
		stage = GOM_GOMII_REFRESH_INIT;
		break;

	default:
		if (cmd_line)
		{
			/* Command line only options */
			switch (action)
			{
			case 'Y':
				/* Ignoring configuration; ignored here. */
				gom_info(GOM_INFO_ERROR, "Y is ignored here; it must be given before any other option (except q or v).");
				break;
			case 'i':
				/* Explicit gomii startup */
				local_optarg = get_option_arg("Gomii to start (t|i)? ");

				if (strcmp(local_optarg, "t") == 0)
				{
					gom_iface_t();
				}
				else if (strcmp(local_optarg, "x") == 0)
				{
#ifdef GOM_ENABLE_X_GOMII
					gom_iface_x();
#else
					gom_info(GOM_INFO_ERROR, "This binary of gom hasn't the X gomii compiled in.");
#endif
				}
				else
					gom_info(GOM_INFO_ERROR, "Unknown gomii shorthand: %s.", local_optarg);
				break;

			case 'q':
				/* Set verbosity to quiet */
				gom_info_verbosity_set(GOM_INFO_NORMAL, GOM_INFO_QUIET);
				break;

			case 'v':
				/* Set verbosity to any, or increase if no option args */
				local_optarg = get_option_arg("New verbosity level? ");

				if (local_optarg)
					gom_info_verbosity_set(GOM_INFO_NORMAL, atoi(local_optarg));
				else
					gom_info_verbosity_set(GOM_INFO_NORMAL, gom_info_verbosity()+1);
				break;

			case 'x':
				/* Xtract mixer settings as command line switches (for scripts etc.) */
				gom_file_settings_write(stdout, 0);
				break;

			case 'I':
				/* Read options from stdin */
				gom_file_options_read(GOM_INFO_NORMAL, stdin, NULL);
				break;

				/* ********************* */
				/* NEW --print-*-options */

			case '1':
				/* --print-driver */
				printf("%s", gom_driver());
				break;

			case '2':
				/* --print-driver-channels */
				for (c = gom_driver_C_first(); c > -1; c = gom_driver_C_next(c, 1))
					printf("%d ", c);
				break;

			case '3':
				/* --print-device */
				printf("%s", gom_driver_d());
				break;

			case '4':
				/* --print-channels */
				for (c = gom_driver_c_first(); c > -1; c = gom_driver_c_next(c, 1))
					printf("%d ", c);
				break;

			case '5':
				/* --print-channel-name */
				printf("%s", gom_driver_c_name(gom_mixer_c_current(), 0));
				break;

			case '6':
				/* --print-channel-label */
				printf("%s", gom_driver_c_name(gom_mixer_c_current(), 1));
				break;

			case '7':
				/* --print-channel-volumes */
				for (cc = gom_driver_c_c_first(gom_mixer_c_current()); cc > -1;
						 cc = gom_driver_c_c_next(gom_mixer_c_current(), cc, 1))
					printf("%d ", cc);
				break;

			case '8':
				/* --print-channel-volume */
				printf("%d", gom_driver_c_c_v(gom_mixer_c_current(), gom_mixer_c_c_current()));
				break;

			case '9':
				/* --print-channel-recsrc */
				printf("%d", gom_driver_c_r(gom_mixer_c_current()));
				break;

			case '0':
				/* --print-channel-lock */
				printf("%d", gom_mixer_c_l(gom_mixer_c_current()));
				break;

			case 'a':
				/* --print-fade-interval */
				printf("%d", gom_mixer_fadival());
				break;

			case 'A':
				/* --print-refresh-interval */
				printf("%d", gom_gomii_refival());
				break;

				/* ********************* */
				/* Odds && Ends */

			case '?':
				/* bad getopt syntax */
				gom_info(GOM_INFO_ERROR, "Bad command line option syntax.");
				gom_info(GOM_INFO_QUIET, GOM_ACTION_HELPHINT);
				break;

			default:
				/* Unknown action, we have to catch the zero char */
				gom_info(GOM_INFO_ERROR, "\"%c\"=%d: Unknown action.",
								 (action == 0) ? ' ' : action, action);
				gom_info(GOM_INFO_QUIET, GOM_ACTION_HELPHINT);
				break;
			}
		}
		else
		{
			/* Unknown action, we have to catch the zero char */
			gom_info(GOM_INFO_ERROR, "\"%c\"=%d: Unknown action.",
							 (action == 0) ? ' ' : action, action);
			stage = GOM_GOMII_REFRESH_NO;
		}
	}
	return stage;
}
