/* gom, Gom is nOt yet another (Audio) Mixer.
 *
 * Copyright (C) 1996-2004  Stephan Sürken <absurd@olurdix.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * gom_action: options, standard action
 */

/*
 * INCLUDES
 */

/*
 * MACROS
 */

/*
 * DECLARATIONS/DEFINTIONS
 */
#define GOM_ACTION_HELPHINT "Try `gom -h, --help, -H, --help-verbose' or `man gom' for help."

/* without GNU getopt_long, gom uses standard getopt(), but needs
   the long version for defining still */
#if !defined(HAVE_GETOPT_H)
struct option
{
  const char *name;
  int has_arg;
  int *flag;
  int val;
};
#define no_argument             0
#define required_argument       1
#define optional_argument       2
#endif /* not HAVE_GETOPT_H */

/*
 * FUNCTION PROTOTYPES
 */

/*****************/

char *
gom_action_shortopts();

int
gom_action_getopt(int argc, char * const argv[]);

void
gom_action_help_special_set(char * help_text[]);

enum gom_gomii_refresh_stage
gom_action(enum gom_info_types std_verb,
	   char action, int cmd_line,
	   char * (* get_option_arg) (char *),
	   void (* pre_scroll) (), void (* post_scroll) ());
