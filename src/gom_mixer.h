/* gom, Gom is nOt yet another (Audio) Mixer.
 *
 * Copyright (C) 1996-2004  Stephan Sürken <absurd@olurdix.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * gom_mixer.c: gom-special mixer functions
 */

/*
 * INCLUDES
 */

/*
 * MACROS
 */

/*
 * DECLARATIONS/DEFINTIONS
 */

typedef
struct
{
  int volume[GOM_DRIVER_CHANNELS][GOM_DRIVER_CHANNEL_CHANNELS];
  int record[GOM_DRIVER_CHANNELS];
} Gom_mixer_settings;

/** MIXER */
void gom_mixer_d_defaults_set(enum gom_info_types std_verb);

/** FIND CHANNEL FROM NAME */
int gom_mixer_C_find(char * name);

/** CURRENT CHANNEL/VOLUME: just holds channel/volume + integrity checks. */
int gom_mixer_c_current();
int gom_mixer_c_c_current();
void gom_mixer_c_current_set(enum gom_info_types std_verb, int c);
void gom_mixer_c_c_current_set(enum gom_info_types std_verb, int cc);

/** MUTE */
void gom_mixer_c_mute(enum gom_info_types std_verb, int c);
void gom_mixer_mute(enum gom_info_types std_verb);

/** FADE */
int gom_mixer_fadival();
int gom_mixer_fadival_set(enum gom_info_types std_verb, int duration);
void gom_mixer_c_c_v_fade(enum gom_info_types std_verb, int c, int cc, int to_volume);

/* Znapshot settings */
void gom_mixer_snapshot(enum gom_info_types std_verb);
void gom_mixer_unsnapshot(enum gom_info_types std_verb);

/** Lock channel. */
int gom_mixer_c_l(int c);
void gom_mixer_c_l_set(enum gom_info_types std_verb, int c, int lock);

/** set all locks, gom's config of set_volumes */
void gom_mixer_l_set(enum gom_info_types std_verb, int lock);

/** Setting volumes: standard */
void gom_mixer_c_v_set(enum gom_info_types std_verb, int c, int volume);

/** Setting volume: lock-sensible */
void gom_mixer_c_c_v_set_l(enum gom_info_types std_verb, int c, int cc, int volume);

/** Fading volume: lock-sensible */
void gom_mixer_c_c_v_fade_l(enum gom_info_types std_verb, int c, int cc, int volume);

/** Setting recording: single */
void gom_mixer_c_r_set_single(enum gom_info_types std_verb, int c);
